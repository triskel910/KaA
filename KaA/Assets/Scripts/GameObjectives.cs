﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectives : MonoBehaviour {

    // Use this for initialization
    public static GameObjectives Instance;


    public bool EquipPistol;
    public bool EquipRifle;

    public bool HidePistol = true;
    public bool HideRifle = true;

    //objectives
    public bool haveCheckPC1;
    public bool havePickPistol;

    public bool haveCheckConsoleDoor1;
    public bool haveKey;
    public bool canOpenGlassDoor;

    public bool AdvanceSquad;

    public bool Cantmovecam;
    public bool looktatGolem;
    public bool GoFullBlack;
    public bool isPause;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }

    }

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
