﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAction : MonoBehaviour {

    // Use this for initialization
    public GameObject ActionE;

    public GameObject Pistol;
    public GameObject Rifle;
    public GameObject Gpistol;
    public GameObject Grifle;
    public GameObject RifleAmmoBoxes;

    public GameObject Desk;
    public Text DialogueText;
    public Image blackScreen;
    private float FadeSpeed = 1.0f;

    public Animator FadeScreen;

    private float ActionDistance = 2.0f;

    public GameObject wakeUpCam;
    public GameObject PlayerCam;
    public GameObject Cam1;
    public GameObject Cam2;
    public GameObject Cam3;
    public GameObject CamDeadbody;
    public GameObject CamParking;

    public Animator ParkingDoor;

    public GameObject AlertSound;
    public GameObject MoveTutorial;

    public GameObject Key;

    private bool CanGrabPistol;
    private bool havePistol;
    private bool CanGrabPistolAmmo;

    private bool canGrabRifle;


    private bool canTriggerPcConsole;
    private bool haveTriggerPcConsole;

    private bool CantriggerButton;
    private bool haveTriggerButton;
    private bool CanUseCar;

    private bool OpenGlassDoor;

    private bool canGrabKey;

    private bool CanViewCams1;
    private bool ShowOnceCams1Once = true;

    public float PlayerHP = 100.0f;
    private float InitialPlayerHP;
    public Slider HPSlider;

  
    public GameObject SpawnGolem;
    //public Vector3 Golem;
    public GameObject FullBlack;

    public GameObject PauseMenu;

    void Start() {

        Pistol.gameObject.SetActive(false);
        Rifle.gameObject.SetActive(false);

        ActionE.gameObject.SetActive(false);

        PlayerCam.SetActive(false);
        Cam1.SetActive(false);
        Cam2.SetActive(false);
        Cam3.SetActive(false);
        CamDeadbody.SetActive(false);

        CamParking.SetActive(false);

        MoveTutorial.SetActive(false);

        FullBlack.SetActive(false);

        StartCoroutine(ChangeToPlayerCam(8.0f));
        StartCoroutine(FirstDialogueLine(1.0f));

        PlayerMove.walkSpeed = 0.0f;
        HPSlider = HPSlider.GetComponent<Slider>();
        HPSlider.value = PlayerHP;
        InitialPlayerHP = PlayerHP;

        PauseMenu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update() {
     //   Debug.Log("HP: " + PlayerHP);
        HPSlider.value = PlayerHP;

        /* GrabPistol();
         GrabAmmo();*/

        if (ShowOnceCams1Once)
        {
            if ((Desk.transform.position - this.transform.position).sqrMagnitude <= ActionDistance * ActionDistance)
            {

                if (!ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(true);
                    CanViewCams1 = true;

                }

            }
            else
            {
                ActionE.gameObject.SetActive(false);
                CanViewCams1 = false;

            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (CanViewCams1)
            {
                MoveTutorial.SetActive(false);
                AlertSound.SetActive(false);
                if (ShowOnceCams1Once)
                {
                    ActivatePC1();
                    ShowOnceCams1Once = false;
                }
            }

        }
        // if (Input.GetKeyDown(KeyCode.E))
        //{
        if (CanGrabPistol)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Pistol.SetActive(true);
                Destroy(Gpistol);
                havePistol = true;
                GameObjectives.Instance.havePickPistol = true;
                if (ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(false);
                }
                CanGrabPistol = false;

            }
        }
        if (canGrabRifle)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Pistol.SetActive(false);
                Rifle.SetActive(true);
                Destroy(Grifle);
                Destroy(RifleAmmoBoxes);
                Gun.MaxAmmo = 240;
                if (ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(false);
                }
                canGrabRifle = false;

            }
        }

        if (canGrabKey)
        {
            if (Input.GetKeyDown(KeyCode.E))

            {
                GameObjectives.Instance.haveKey = true;
                Destroy(Key);

                if (ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(false);
                }

                canGrabKey = false;
            }
        }
        if (canTriggerPcConsole)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                StartCoroutine(FithteenDialogueLine(4.0f));
                GameObjectives.Instance.haveCheckConsoleDoor1 = true;
                if (ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(false);
                }
                haveTriggerPcConsole = true;
                canTriggerPcConsole = false;
            }

          
        }
        if(OpenGlassDoor)
        {           
            if (Input.GetKeyDown(KeyCode.E))
            {
                GameObjectives.Instance.canOpenGlassDoor = true;
                if (ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(false);
                }
            }
        }

        if(CantriggerButton)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayerCam.SetActive(false);
                CamParking.SetActive(true);
                if (ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(false);
                }
                StartCoroutine(switchpackingtoplayercam(14.0f));
                StartCoroutine(ParkiingDoor(1.5f));
                haveTriggerButton = true;
                CantriggerButton = false;
            }
          
        }
        if(CanUseCar)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                GameObjectives.Instance.Cantmovecam = true;
                GameObjectives.Instance.looktatGolem = true;
                GameObject emptyGameImpact = Instantiate(SpawnGolem, SpawnGolem.transform.position, Quaternion.LookRotation(this.transform.position));
                StartCoroutine(BlackScreen(2.5f));
                CanUseCar = false;
            }

        }

    

        RegenHP();
        Pausa();
    }

    void ActivatePC1()
    {
        GameObjectives.Instance.haveCheckPC1 = true;
        ActionE.gameObject.SetActive(false);
        PlayerCam.SetActive(false);
        Cam1.SetActive(true);
        StartCoroutine(ShowCamera1(3.0f));
    //    PlayerMove.stopStepSound = true;
    }

    void RegenHP()
    {
        if (HPSlider.value < InitialPlayerHP)
        {
            StartCoroutine(RegenHealth(8.0f));
        }
    }

    public void TakeDamage(float amount)
    {
        PlayerHP -= amount;

        if (PlayerHP <= 0f)
        {
            StartCoroutine(Death(3.0f));
            PlayerMove.walkSpeed = 0.0f;
            FadeScreen.gameObject.GetComponent<Animator>().SetBool("PlayerDie", true);

        }
    }

    void Pausa()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (!GameObjectives.Instance.isPause)
            {
                GameObjectives.Instance.isPause = true;
            }else
            {
                GameObjectives.Instance.isPause = false;
            }
           
        }

        if(GameObjectives.Instance.isPause)
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                PauseMenu.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;

            }
           
        }
        if (!GameObjectives.Instance.isPause)
        {
           if(Time.timeScale ==0)
            {
                Time.timeScale = 1;
                Cursor.lockState = CursorLockMode.Locked;
                PauseMenu.SetActive(false);
                Cursor.visible = false;

            }
        }
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TriggerDeadBody")
        {
            PlayerMove.walkSpeed = 0.0f;
        //    PlayerMove.stopStepSound = true;
            StartCoroutine(ShowCameraDeadBody(3.0f));
            StartCoroutine(NineDialogueLine(0.2f));
            PlayerCam.SetActive(false);
            CamDeadbody.SetActive(true);
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "Pistol")
        {
            CanGrabPistol = true;
            if (!ActionE.gameObject.activeInHierarchy)
            {
                ActionE.gameObject.SetActive(true);
            }

        }
        if (other.gameObject.tag == "Rifle")
        {
            canGrabRifle = true;
            if (!ActionE.gameObject.activeInHierarchy)
            {
                ActionE.gameObject.SetActive(true);
            }

        }
        if (other.gameObject.tag == "PistolAmmo")
        {

            Gun.MaxAmmo += 20;
            Destroy(other.gameObject);

        }
        if(other.gameObject.tag == "TriggerDialaogue1")
        {
            StartCoroutine(ElevenDialogueLine(1.0f));
        }
        if (other.gameObject.tag == "DoorConsole1")
        {
            if (!haveTriggerPcConsole)
            {
                StartCoroutine(ThirdteenDialogueLine(0.0f));
                canTriggerPcConsole = true;

                if (!ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(true);
                }
               
            }
        }
        if (other.gameObject.tag == "Key")
        {
            canGrabKey = true;
            if (!ActionE.gameObject.activeInHierarchy)
            {
                ActionE.gameObject.SetActive(true);
            }
        }
        if (other.gameObject.tag == "Button")
        {
            CantriggerButton = true;
            if (!ActionE.gameObject.activeInHierarchy)
            {
                ActionE.gameObject.SetActive(true);
            }
        }

        if (other.gameObject.tag == "GlassDoor")
        {
            if (GameObjectives.Instance.haveKey)
            {
                OpenGlassDoor = true;
                if (!ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(true);
                }
            }
        }
        if (other.gameObject.tag == "Car")
        {
            if (haveTriggerButton)
            { 
                CanUseCar = true;
            if (!ActionE.gameObject.activeInHierarchy)
            {
                ActionE.gameObject.SetActive(true);
            }
        }
        }

    }
    private void OnTriggerExit(Collider exit)
    {
        if (exit.gameObject.tag == "Pistol")
        {
            CanGrabPistol = false;
            if (ActionE.gameObject.activeInHierarchy)
            {
                ActionE.gameObject.SetActive(false);
            }


        }
        if (exit.gameObject.tag == "Rifle")
        {
            canGrabRifle = false;
            if (ActionE.gameObject.activeInHierarchy)
            {
                ActionE.gameObject.SetActive(false);
            }


        }
        if (exit.gameObject.tag == "DoorConsole1")
        {
            if (!haveTriggerPcConsole)
            {
                canTriggerPcConsole = false;

                if (ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(false);
                }
            }
        }

        if (exit.gameObject.tag == "Key")
        {
            canGrabKey = false;
            if (ActionE.gameObject.activeInHierarchy)
            {
                ActionE.gameObject.SetActive(false);
            }
        }
        if (exit.gameObject.tag == "GlassDoor")
        {
            if (GameObjectives.Instance.haveKey)
            {
                OpenGlassDoor = false;
                if (ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(false);
                }
            }
        }
        if (exit.gameObject.tag == "Button")
        {
           
                CantriggerButton = false;
                if (ActionE.gameObject.activeInHierarchy)
                {
                    ActionE.gameObject.SetActive(false);
                }
            
        }
        if (exit.gameObject.tag == "Car")
        {

            CanUseCar = false;
            if (ActionE.gameObject.activeInHierarchy)
            {
                ActionE.gameObject.SetActive(false);
            }

        }

    }

    IEnumerator BlackScreen(float sec)
    {
        yield return new WaitForSeconds(sec);

        FullBlack.SetActive(true);
        StartCoroutine(EndGame(2.0f));

    }
    IEnumerator EndGame(float sec)
    {
        yield return new WaitForSeconds(sec);

        Application.LoadLevel(2);

    }

    IEnumerator switchpackingtoplayercam(float sec)
    {
        yield return new WaitForSeconds(sec);

        CamParking.SetActive(false);
        PlayerCam.SetActive(true);

    }



    IEnumerator RegenHealth(float sec)
    {
        yield return new WaitForSeconds(sec);

        PlayerHP = PlayerHP + Time.deltaTime*2;

    }
    IEnumerator ParkiingDoor(float sec)
    {
        yield return new WaitForSeconds(sec);
        ParkingDoor.gameObject.GetComponent<Animator>().SetBool("OpenDoor", true);
        GameObjectives.Instance.AdvanceSquad = true;
    }

    IEnumerator Death(float sec)
    {
        yield return new WaitForSeconds(sec);
        Application.LoadLevel(1);
    }


        IEnumerator FirstDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "Where am I?";

        StartCoroutine(SecondDialogueLine(4.0f));
    }
    IEnumerator SecondDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "";
        StartCoroutine(ThirdDialogueLine(4.0f));


    }
    IEnumerator ThirdDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "Was I dreaming?";
        StartCoroutine(FourthdDialogueLine(4.0f));


    }
    IEnumerator FourthdDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "";
        StartCoroutine(FithDialogueLine(0.5f));

    }
    IEnumerator FithDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "What is that noise?";
        StartCoroutine(SixDialogueLine(2.0f));

    }
    IEnumerator SixDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "";
        MoveTutorial.SetActive(true);
        PlayerMove.walkSpeed = 6.0f;
      //  PlayerMove.stopStepSound = false;

    }
    IEnumerator SevenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "What happened here?";
        StartCoroutine(EightDialogueLine(2.0f));

    }
    IEnumerator EightDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "";
        
    }

    IEnumerator NineDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "I should grab his gun just in case";
        StartCoroutine(TenDialogueLine(2.0f));

    }
    IEnumerator TenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "";

    }
    IEnumerator ElevenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "Who are they?";
        StartCoroutine(TwelveDialogueLine(2.0f));

    }
    IEnumerator TwelveDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "";

    }

    IEnumerator ThirdteenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "I could use this console to open this door";

        StartCoroutine(FourthteenDialogueLine(2.0f));
    }
    IEnumerator FourthteenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "";


    }
    IEnumerator FithteenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "I need to find a way to get out from here";
        StartCoroutine(SixteenDialogueLine(2.0f));
    }
    IEnumerator SixteenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "";
        StartCoroutine(SeventeenDialogueLine(4.0f));

    }
    IEnumerator SeventeenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "I could access to Command Post and open the garage door";
        StartCoroutine(EighteenDialogueLine(4.0f));
    }
    IEnumerator EighteenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = "Then use one of the trucks to escape";
        StartCoroutine(NineteenDialogueLine(2.5f));

    }
    IEnumerator NineteenDialogueLine(float sec)
    {
        yield return new WaitForSeconds(sec);

        DialogueText.GetComponent<UnityEngine.UI.Text>().text = " ";


    }




    IEnumerator ChangeToPlayerCam(float sec)
    {
        yield return new WaitForSeconds(sec);

        wakeUpCam.SetActive(false);
        PlayerCam.SetActive(true);
       // PlayerMove.stopStepSound = false;

    }
    IEnumerator ShowCamera1(float sec)
    {
        yield return new WaitForSeconds(sec);

        Cam1.SetActive(false);
        Cam2.SetActive(true);
        StartCoroutine(ShowCamera2(3.0f));

    }

    IEnumerator ShowCamera2(float sec)
    {
        yield return new WaitForSeconds(sec);
        Cam2.SetActive(false);
        Cam3.SetActive(true);
        StartCoroutine(ShowCamera3(3.0f));

    }

    IEnumerator ShowCamera3(float sec)
    {
        yield return new WaitForSeconds(sec);
        Cam3.SetActive(false);
        PlayerCam.SetActive(true);
        StartCoroutine(SevenDialogueLine(0.5f));

    }

    IEnumerator ShowCameraDeadBody(float sec)
    {
        yield return new WaitForSeconds(sec);
        CamDeadbody.SetActive(false);
        PlayerCam.SetActive(true);
        PlayerMove.walkSpeed = 6.0f;
       // PlayerMove.stopStepSound = false;

    }

   
}
