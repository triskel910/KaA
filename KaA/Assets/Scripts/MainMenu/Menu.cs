﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    // Use this for initialization

    public GameObject Setings;
    private bool isSettings;

    public AudioListener Sound;
    private bool ShutUp = false;

    


	void Start () {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Setings.SetActive(false);

       
    }
	
	// Update is called once per frame
	void Update () {
		
	}

   public void PlayGame()
    {
        if(!isSettings)
            Application.LoadLevel(1);
    }

    public void QuitGame()
    {
        if(!isSettings)
            Application.Quit();
    }

    public void Settings()
    {
        isSettings = true;
        Setings.SetActive(true);
    }

    public void backMenu()
    {
        isSettings = false;
        Setings.SetActive(false);
    }

    public void MuteGame()
    {
        if (!ShutUp)
            ShutUp = true;
        else
            ShutUp = false;


        if (ShutUp)
            Sound.GetComponent<AudioListener>().enabled = false;
        else
            Sound.GetComponent<AudioListener>().enabled = true;
    }
}
