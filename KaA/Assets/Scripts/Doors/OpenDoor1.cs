﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor1 : MonoBehaviour {

    private Animator d;

    public AudioSource DoorSounds;

    public AudioClip DoorOpen;
    public AudioClip DoorClose;
    // Use this for initialization
    void Start()
    {
        d = this.GetComponent<Animator>();
        DoorSounds = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (GameObjectives.Instance.haveCheckPC1)
            {
                d.SetBool("OpenDoor", true);
                DoorSounds.PlayOneShot(DoorOpen, 1.0f);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (GameObjectives.Instance.haveCheckPC1)
            {
                d.SetBool("OpenDoor", false);
                DoorSounds.PlayOneShot(DoorClose, 1.0f);
            }
        }
    }
}
