﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor2 : MonoBehaviour {

    private Animator d;
    // Use this for initialization

    public AudioSource DoorSounds;

    public AudioClip DoorOpen;
    public AudioClip DoorClose;
    void Start () {
        d = this.GetComponent<Animator>();
        DoorSounds = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (GameObjectives.Instance.havePickPistol)
            {
                d.SetBool("OpenDoor", true);
                DoorSounds.PlayOneShot(DoorOpen, 1.0f);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            d.SetBool("OpenDoor", false);
            DoorSounds.PlayOneShot(DoorClose, 1.0f);
        }
    }
}
