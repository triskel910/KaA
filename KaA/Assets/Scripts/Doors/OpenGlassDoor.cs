﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenGlassDoor : MonoBehaviour {

    private Animator d;
    // Use this for initialization

    public AudioSource DoorSounds;

    public AudioClip DoorOpen;
    public AudioClip DoorClose;

    void Start()
    {
        d = this.GetComponent<Animator>();
        DoorSounds = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (GameObjectives.Instance.haveKey && GameObjectives.Instance.canOpenGlassDoor)
        {
            d.SetBool("OpenDoor", true);
            DoorSounds.PlayOneShot(DoorOpen, 1.0f);
            GameObjectives.Instance.haveKey = false;

        }
    }




}
