﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour {

    public enum gunClass { Pistol, Rifle }

    public gunClass type;

    private Animator GunAnims;
   
    public static int MaxAmmo;
    private int currentRounds;

    private float damage, fireRate, reloadTime;
    private int rounds, maxRounds, roundsUsed;
    private int StartingRounds;
    private float nextTimetofire = 0.0f;
    
    public Camera fpsCam;

    private Transform startPosaim;
    private int aimcount = 0;


    public GameObject impactEffect;
    public GameObject Muzzle;

    public Text ShowCurrentAmmo;

    private bool cantReload;

    public AudioSource GunShots;
    public AudioClip PistolShot;
	// Use this for initialization
	void Start () {

        switch (type)
        {
           

            case gunClass.Pistol:
                damage = 12.0f;
                fireRate = 6.0f;
                rounds = 20;
               
                reloadTime = 1.2f;
                MaxAmmo = 0/*350*/;
                GameObjectives.Instance.EquipPistol = true;
                break;

            case gunClass.Rifle:
                damage = 18.0f;
                fireRate = 8.0f;
                rounds = 30;
               
                reloadTime = 2.4f;
                MaxAmmo = 240;
                GameObjectives.Instance.EquipPistol = false;
                break;

          
        }

        GunAnims = this.gameObject.GetComponent<Animator>();
        GunShots = GetComponent<AudioSource>();

          startPosaim = this.gameObject.transform;

        StartingRounds = rounds;
    }
	
	// Update is called once per frame
	void Update () {

        currentRounds = rounds;

        if (Input.GetMouseButton(0) && Time.time >= nextTimetofire)
        {
           // if (!GameManager.Instance.isBuymenu)
            //{
                cantReload = true;
                if (rounds >= 1)
                {
                    nextTimetofire = Time.time + 1f / fireRate;
                // if(!GameObjectives.Instance.EquipPistol)
                GunAnims.SetBool("Shoot", true);
                roundsUsed++;
                if (GameObjectives.Instance.EquipPistol)
                    GunShots.PlayOneShot(PistolShot,1.0f);
                if(!GameObjectives.Instance.Cantmovecam)
                    Shoot();
                    rounds--;
               // }
            }
        }
        if (Input.GetMouseButtonUp(0))
            cantReload = false;

        if (rounds < 0)
            rounds = Mathf.Clamp(currentRounds, rounds, 0);

        if (MaxAmmo <= 0)
            MaxAmmo = 0;

        Reload();
        Showammo();
         if (Input.GetMouseButtonUp(0))
         {
            GunAnims.SetBool("Shoot", false);
        }
      
    }
    void StopShootAnim()
    {
        GunAnims.SetBool("Shoot", false);
    }


    void Shoot()
    {
        RaycastHit hit;


        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit))
        {
           // Debug.Log(hit.transform.name);

            Target target =  hit.transform.GetComponent<Target>();

            if(target !=null)
            {
                if (hit.collider is CapsuleCollider)
                    target.TakeDamage(damage);
                else if (hit.collider is BoxCollider)
                    target.TakeDamage(damage * 2);

               
            }
            Target2 target2 = hit.transform.GetComponent<Target2>();

            if (target2 != null)
            {
                if (hit.collider is CapsuleCollider)
                    target2.TakeDamage(damage);
                else if (hit.collider is BoxCollider)
                    target2.TakeDamage(damage * 2);


            }


            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * 50.0f);
            }

            GameObject emptyGameImpact = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(emptyGameImpact, 0.1f);


            GameObject emptyGameImpactM = Instantiate(impactEffect, Muzzle.transform.position, Quaternion.LookRotation(hit.normal));
            Destroy(emptyGameImpactM, 0.1f);

        }
    }

    
    void Reload()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            if(!cantReload)
            {

                GunAnims.SetBool("Reload",true);
                StartCoroutine(Reloading(reloadTime));
                cantReload = true;
            }
                
        }
    }
    private IEnumerator Reloading(float time)
    {
        yield return new WaitForSeconds(time);
        GunAnims.SetBool("Reload", false);

        if (rounds == 0 && MaxAmmo >= StartingRounds)
        {
            rounds = StartingRounds;
            MaxAmmo = MaxAmmo - roundsUsed;
        }
        //no ammo in clip, but less than 6 max
        if (rounds == 0 && MaxAmmo < StartingRounds)
        {
            rounds = MaxAmmo;
            MaxAmmo = 0;
        }

        //some ammo in clip, but more than 6 max
        if (rounds < StartingRounds && rounds > 1 && MaxAmmo >= StartingRounds)
        {
            rounds = StartingRounds;
            MaxAmmo = MaxAmmo - roundsUsed;
        }
        //some ammo in clip, but less than 6 max
        if (rounds < StartingRounds && rounds > 1 && MaxAmmo < StartingRounds)
        {
            if (rounds + MaxAmmo == StartingRounds)
            {
                rounds = StartingRounds;
                MaxAmmo = 0;
            }
            if ((rounds + MaxAmmo) < StartingRounds)
            {
                rounds = rounds + MaxAmmo;
                MaxAmmo = 0;
            }
            if ((rounds + MaxAmmo) > StartingRounds)
            {
                rounds = StartingRounds;
                MaxAmmo = MaxAmmo - roundsUsed;
            }
        }
      
          roundsUsed = 0;
         cantReload = false;
    }

    void Showammo()
    {
        ShowCurrentAmmo.GetComponent<UnityEngine.UI.Text>().text = rounds + " / " +MaxAmmo;
    }
}
