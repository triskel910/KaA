﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    CharacterController charControl;
    public static float walkSpeed = 6.0f;
    public AudioSource StepsSound;

   // public static bool stopStepSound = true;

    private Vector3 LastPos ;
    private Vector3 CurrentPos;

    private void Start()
    {
        StepsSound = GetComponent<AudioSource>();

    }
    void Awake()
    {
        charControl = GetComponent<CharacterController>();
    }

    void Update()
    {

    

        MovePlayer();

       
    }
    

    void MovePlayer()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 moveDirSide = transform.right * horiz * walkSpeed;
        Vector3 moveDirForward = transform.forward * vert * walkSpeed;

        charControl.SimpleMove(moveDirSide);
        charControl.SimpleMove(moveDirForward);

       // if(stopStepSound)
            StepsSound.Stop();

        if (Input.GetKeyDown(KeyCode.W) )
        {
          //  if(!stopStepSound)
                StepsSound.Play();
        }
        if (Input.GetKeyUp(KeyCode.W) )
        {
            StepsSound.Stop();
        }

    }
}
