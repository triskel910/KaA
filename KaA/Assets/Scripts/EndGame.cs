﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour {

    // Use this for initialization
    public GameObject DeathScreen;
	void Start () {

        StartCoroutine(Death(11.0f));
        DeathScreen.SetActive(false);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator Death(float sec)
    {
        yield return new WaitForSeconds(sec);
        DeathScreen.SetActive(true);
        StartCoroutine(BackTomenu(4.0f));
    }

    IEnumerator BackTomenu(float sec)
    {
        yield return new WaitForSeconds(sec);
        Application.LoadLevel(0);
    }

}
