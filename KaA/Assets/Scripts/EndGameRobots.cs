﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameRobots : MonoBehaviour {

    // Use this for initialization

    public enum enemyClass {  Robot2 }
    private float health, damage, speed, Ammo;
    public enemyClass type;

    public Animator Zanimations;


    private Vector3 target;


    public GameObject RobotGun;
    public GameObject GunMuzzle;
    private GameObject player;
    public GameObject RollPosition;

    private Vector3 RollVector;

    private bool Move;
   
    private bool Shoot;

    private bool AttackMode;

    private float angle;
    private float Tspeed = 5.0f;
    public Transform TransfPlayer;
    private bool isDead;

    public AudioSource RobotSounds;

    public AudioClip Gunshots;
    public AudioClip AlertSound;

    public GameObject impactEffect;
    // Use this for initialization
    void Start()
    {
        switch (type)
        {           

            case enemyClass.Robot2:
                health = 100.0f;
                damage = 4.0f;
                speed = 1.8f;

                Ammo = 20.0f;
                break;

        }

        StartCoroutine(Death(10.0f));
      

        // anim = GetComponent<Animation>();

        player = (GameObject)GameObject.FindGameObjectWithTag("Player");
        
        RobotSounds = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        attackPlayer();


       
    }

  
   

    private void attackPlayer()
    {

        if (AttackMode)
        {
            Zanimations.SetBool("Interact", false);
            Zanimations.SetBool("Shoot", true);

            //transform.LookAt(target);
            if (!isDead)
            {
                Vector3 relative = transform.InverseTransformPoint(TransfPlayer.position);
                angle = Mathf.Atan2(relative.x, relative.z) * Mathf.Rad2Deg * Time.deltaTime * Tspeed;
                transform.Rotate(0, angle, 0);
            }
        }
        if (AttackMode && Ammo <= 0)
        {
            Zanimations.SetBool("Reload", true);
        }

    }
    public void ShootGun()
    {
        Ammo--;
        RaycastHit hit;

        if (Physics.Raycast(RobotGun.transform.position, RobotGun.transform.forward, out hit))
        {
            // Debug.Log(hit.transform.name);


            PlayerAction FPSPlayer = hit.transform.GetComponent<PlayerAction>();

            if (FPSPlayer != null)
            {
                if (hit.collider is CapsuleCollider)
                    FPSPlayer.TakeDamage(damage);


            }


            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * 50.0f);
            }

            Debug.DrawLine(RobotGun.transform.position, hit.point, Color.red);


        }
        RobotSounds.PlayOneShot(Gunshots, 1.0f);

        GameObject emptyGameImpact = Instantiate(impactEffect, GunMuzzle.transform.position, Quaternion.LookRotation(hit.normal));
        Destroy(emptyGameImpact, 0.1f);
    }


    
    void StopHitAnim()
    {
        Zanimations.SetBool("TakeDamage", false);
    }
    void StopRollAnim()
    {
        Zanimations.SetBool("Roll", false);

    }

    void HaveReload()
    {
        Zanimations.SetBool("Reload", false);
        Ammo = 20.0f;
    }

    IEnumerator Death(float sec)
    {
        yield return new WaitForSeconds(sec);
        AttackMode = true;
    }

}
