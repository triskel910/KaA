﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Target2 : MonoBehaviour {

    public enum enemyClass { Robot, RobotS }
    private float health, damage, speed, Ammo;
    public enemyClass type;

    public Animator Zanimations;


    private NavMeshAgent nav;
    private Vector3 target;


    public GameObject RobotGun;
    public GameObject GunMuzzle;
    private GameObject player;
    public GameObject RollPosition;

    public GameObject PatrolPoint1;
    public GameObject PatrolPoint2;

    public GameObject StopPosition;

    private Vector3 Patrol1;
    private Vector3 Patrol2;

    private Vector3 StopPos;

    private bool Move;
    private bool Roll;
    private bool Shoot;

    private bool AttackMode;
    private bool isPatrol = true;
    private bool ispatrol1;
    private bool ispatrol2;

    private bool isSquad;


    private float angle;
    private float Tspeed = 5.0f;
    public Transform TransfPlayer;
    private bool isDead;

    public AudioSource RobotSounds;

    public AudioClip Gunshots;
    public AudioClip AlertSound;

    public GameObject impactEffect;
    // Use this for initialization
    void Start()
    {
        switch (type)
        {
            case enemyClass.Robot:
                health = 100.0f;
                damage = 4.0f;
                speed = 1.8f;
                Zanimations.SetBool("Walk", true);
                Ammo = 20.0f;

                break;


                case enemyClass.RobotS:
                health = 100.0f;
                damage = 2.0f;
                speed = 1.8f;
                Zanimations.SetBool("Walk", true);
                Ammo = 20.0f;
                isSquad = true;

                break;

        }


        nav = GetComponent<NavMeshAgent>();

        // anim = GetComponent<Animation>();

        player = (GameObject)GameObject.FindGameObjectWithTag("Player");

        Patrol1 = new Vector3(PatrolPoint1.transform.position.x, PatrolPoint1.transform.position.y, PatrolPoint1.transform.position.z);
        Patrol2 = new Vector3(PatrolPoint2.transform.position.x, PatrolPoint2.transform.position.y, PatrolPoint2.transform.position.z);

        if(isSquad)
            StopPos = new Vector3(StopPosition.transform.position.x, StopPosition.transform.position.y, StopPosition.transform.position.z);

        if(!isSquad)
            nav.SetDestination(Patrol1);
        ispatrol1 = true;
        RobotSounds = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        attackPlayer();


        // target = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);

        Patrol();



    }
    void Patrol()
    {
        if (!isSquad)
        { 
        if (nav.remainingDistance <= nav.stoppingDistance)
        {

            if (!nav.hasPath || Mathf.Abs(nav.velocity.sqrMagnitude) < float.Epsilon)
            {


                if (isPatrol)
                {
                    if (ispatrol1)
                    {
                        nav.SetDestination(Patrol1);
                        ispatrol2 = true;
                        ispatrol1 = false;
                    }
                    else if (ispatrol2)
                    {
                        nav.SetDestination(Patrol2);
                        ispatrol2 = false;
                        ispatrol1 = true;
                    }
                }

            }

        }
    }
        if (isSquad)
        {
            if (GameObjectives.Instance.AdvanceSquad)
            {
                nav.SetDestination(StopPos);
              /* if (nav.remainingDistance <= nav.stoppingDistance)
                {

                    if (!nav.hasPath || Mathf.Abs(nav.velocity.sqrMagnitude) < float.Epsilon)
                    {
                        Zanimations.SetBool("Walk", false);
                    }
                }
               else
                    Zanimations.SetBool("Walk", true);*/

            }
        }
    }

    public void TakeDamage(float amount)
    {
        isPatrol = false;
        this.GetComponent<NavMeshAgent>().enabled = false;
        if (!AttackMode)
            AttackMode = true;
        health -= amount;
        Zanimations.SetBool("TakeDamage", true);

        if (health <= 0f)
        {

            Die();
        }

    }
    public void Die()
    {
        this.gameObject.GetComponent<Collider>().enabled = false;

        nav.SetDestination(this.gameObject.transform.position);


        Zanimations.SetBool("Die", true);

        speed = 0.0f;
        isDead = true;
        
    }

    private void attackPlayer()
    {

        if (AttackMode)
        {
            
            Zanimations.SetBool("Shoot", true);
            this.GetComponent<NavMeshAgent>().enabled = false;

            
            if (!isDead)
            {
                Vector3 relative = transform.InverseTransformPoint(TransfPlayer.position);
                angle = Mathf.Atan2(relative.x, relative.z) * Mathf.Rad2Deg * Time.deltaTime * Tspeed;
                transform.Rotate(0, angle, 0);
            }
        }
        if (AttackMode && Ammo <= 0)
        {
            Zanimations.SetBool("Reload", true);
        }

    }
    public void ShootGun()
    {
        Ammo--;
        RaycastHit hit;

        if (Physics.Raycast(RobotGun.transform.position, RobotGun.transform.forward, out hit))
        {
            // Debug.Log(hit.transform.name);


            PlayerAction FPSPlayer = hit.transform.GetComponent<PlayerAction>();

            if (FPSPlayer != null)
            {
                if (hit.collider is CapsuleCollider)
                    FPSPlayer.TakeDamage(damage);


            }


            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * 50.0f);
            }

            Debug.DrawLine(RobotGun.transform.position, hit.point, Color.red);


        }
        RobotSounds.PlayOneShot(Gunshots, 1.0f);

        GameObject emptyGameImpact = Instantiate(impactEffect, GunMuzzle.transform.position, Quaternion.LookRotation(hit.normal));
        Destroy(emptyGameImpact, 0.1f);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (isSquad)
            {
                if (GameObjectives.Instance.AdvanceSquad)
                {
                    if (!AttackMode)
                        RobotSounds.PlayOneShot(AlertSound, 1.0f);
                    AttackMode = true;
                }
            }
            if (!isSquad)
            {
               
                    if (!AttackMode)
                        RobotSounds.PlayOneShot(AlertSound, 1.0f);
                    AttackMode = true;
                
            }
        }
        if (other.gameObject.tag == "Rrstop")
        {
            Zanimations.SetBool("Walk", false);
        }

    }
    void StopHitAnim()
    {
        Zanimations.SetBool("TakeDamage", false);
    }
    void StopRollAnim()
    {
        Zanimations.SetBool("Roll", false);

    }

    void HaveReload()
    {
        Zanimations.SetBool("Reload", false);
        Ammo = 20.0f;
    }
}
