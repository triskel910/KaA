﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour {

    // Use this for initialization
    public AudioListener Sound;
    private bool ShutUp = false;
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
    }
 public void BackTomenu()
    {

        Application.LoadLevel(0);
    }
    public void Mute()
    {
        if (!ShutUp)
            ShutUp = true;
        else
            ShutUp = false;


        if (ShutUp)
            Sound.GetComponent<AudioListener>().enabled = false;
        else
            Sound.GetComponent<AudioListener>().enabled = true;

    }
}
