﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Target : MonoBehaviour {

    public enum enemyClass { Robot, Robot2 }
    private float health, damage, speed, Ammo;
    public enemyClass type;

    public Animator Zanimations;
    

    private NavMeshAgent nav;
    private Vector3 target;

    
    public GameObject RobotGun;
    public GameObject GunMuzzle;
    private GameObject player;  
    public GameObject RollPosition;

    private Vector3 RollVector;

    private bool Move;
    private bool Roll;
    private bool Shoot;

    private bool AttackMode;

    private float angle;
    private float Tspeed = 5.0f;
    public Transform TransfPlayer;
    private bool isDead;

    public AudioSource RobotSounds;

    public AudioClip Gunshots;
    public AudioClip AlertSound;

    public GameObject impactEffect;
    // Use this for initialization
    void Start () {
        switch (type)
        {
            case enemyClass.Robot:
                health = 100.0f;
                damage = 4.0f;
                speed = 1.8f;
                Zanimations.SetBool("Interact", true);
                Ammo = 20.0f;
                break;

            case enemyClass.Robot2:
                health = 100.0f;
                damage = 4.0f;
                speed = 1.8f;
               
                Ammo = 20.0f;
                break;

        }

       
        nav = GetComponent<NavMeshAgent>();

        // anim = GetComponent<Animation>();

        player = (GameObject)GameObject.FindGameObjectWithTag("Player");

        RollVector =  new Vector3(RollPosition.transform.position.x, RollPosition.transform.position.y, RollPosition.transform.position.z);

        RobotSounds = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
void Update ()
    {
        attackPlayer();
      

        target = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
    }

    public void TakeDamage(float amount)
    {
        if(!AttackMode)
            AttackMode = true;
        health -= amount;
        Zanimations.SetBool("TakeDamage", true);

        if (health <= 0f)
        {
           
            Die();
        }
        
    }
    public void Die()
    {
        this.gameObject.GetComponent<Collider>().enabled = false;

        nav.SetDestination(this.gameObject.transform.position);

      
        Zanimations.SetBool("Die", true);

        speed = 0.0f;
        isDead = true;
       // Destroy(gameObject, 2.5f);
    }

    private void attackPlayer()
    {
        
      if(AttackMode)
        {
            Zanimations.SetBool("Interact", false);
            Zanimations.SetBool("Shoot", true);
            
            //transform.LookAt(target);
            if (!isDead)
            {
                Vector3 relative = transform.InverseTransformPoint(TransfPlayer.position);
                angle = Mathf.Atan2(relative.x, relative.z) * Mathf.Rad2Deg * Time.deltaTime * Tspeed;
                transform.Rotate(0, angle, 0);
            }
        }
      if(AttackMode && Ammo<=0)
        {
            Zanimations.SetBool("Reload", true);
        }

    }
    public void ShootGun()
    {
        Ammo--;
        RaycastHit hit;

        if (Physics.Raycast(RobotGun.transform.position, RobotGun.transform.forward, out hit))
        {
            // Debug.Log(hit.transform.name);

          
            PlayerAction FPSPlayer = hit.transform.GetComponent<PlayerAction>();

            if (FPSPlayer != null)
            {
                if (hit.collider is CapsuleCollider)
                    FPSPlayer.TakeDamage(damage);              


            }


            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * 50.0f);
            }

            Debug.DrawLine(RobotGun.transform.position, hit.point, Color.red);


        }
        RobotSounds.PlayOneShot(Gunshots, 1.0f);

        GameObject emptyGameImpact = Instantiate(impactEffect, GunMuzzle.transform.position, Quaternion.LookRotation(hit.normal));
        Destroy(emptyGameImpact, 0.1f);
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (!AttackMode)
                RobotSounds.PlayOneShot(AlertSound, 1.0f);
            AttackMode = true;            
        }
    }
    void StopHitAnim()
    {
        Zanimations.SetBool("TakeDamage", false);
    }
    void StopRollAnim()
    {
        Zanimations.SetBool("Roll", false);
       
    }

    void HaveReload()
    {
        Zanimations.SetBool("Reload", false);
        Ammo = 20.0f;
    }
}
